<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    use HasFactory;
    protected $table = 'films';
    protected $primaryKey = 'id';

    protected $fillable = ['title', 'director', 'release_year','genre', 'synopsis', 'duration', 'cover_image_url', 'trailer', 'min_rate'];
}
