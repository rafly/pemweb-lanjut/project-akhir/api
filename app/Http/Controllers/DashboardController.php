<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Films;
use App\Models\Cast;
use App\Models\Reviews;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $films = Films::all();
        $films_rating = Films::select('films.*', \DB::raw('ROUND((SELECT AVG(rating) FROM reviews WHERE reviews.film_id = films.id), 2) as avg_rating'))
            ->orderByDesc('avg_rating')
            ->get();
    
        $topRatedFilms = $films_rating->take(3);
    
        return response()->json([
            'films' => $films,
            'topRatedFilms' => $topRatedFilms
        ]);
    }

    public function detail($id)
    {
        $film = Films::findOrFail($id);
        $pemerans = Cast::where('id_film', $id)->get();
        $reviews = Reviews::where('film_id', $id)
            ->join('users', 'reviews.user_id', '=', 'users.id')
            ->select('reviews.*', 'users.name as user_name')
            ->get();

        // Menghitung nilai rata-rata dari rating
        $average_rating = round(Reviews::where('film_id', $id)->avg('rating'), 2);
        
        return response()->json([
            'film' => $film,
            'pemerans' => $pemerans,
            'reviews' => $reviews,
            'average_rating' => $average_rating
        ]);
    }

    public function store(Request $request)
    {
        // Validasi input
        $request->validate([
            'review_text' => 'required|string',
            'film_id' => 'required',
            'rating' => 'required'
        ]);

        // Pastikan bahwa pengguna telah login
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized. Please login to leave a review.'], 401);
        }

        // Dapatkan ID pengguna yang terautentikasi
        $id = Auth::id();
        $filmId = $request->film_id;

        // Periksa apakah pengguna sudah memberikan review untuk film tertentu sebelumnya
        $existingReview = Reviews::where('user_id', $id)->where('film_id', $filmId)->first();

        if ($existingReview) {
            return response()->json(['error' => 'You have already reviewed this film. Cannot review again.'], 400);
        }

        // Gabungkan ID pengguna dan tanggal review dengan permintaan
        $request->merge(['user_id' => $id]);
        $request->merge(['review_date' => now()]);

        try {
            // Simpan data review ke database
            Reviews::create($request->all());
            
            return response()->json(['message' => 'Comment success to publish!'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to publish comment.'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $film = Films::findOrFail($id);
        $film->update($request->all());

        return response()->json(['message' => 'Film updated successfully', 'film' => $film]);
    }

    public function storeMovie(Request $request)
    {
        // Validasi data input
        $request->validate([
            'title' => 'required|string',
            'director' => 'required|string',
            'release_year' => 'required|integer',
            'synopsis' => 'required|string',
            'duration' => 'required|integer',
            'cover_image_url' => 'required|url',
            'trailer' => 'required|url',
            'min_rate' => 'required|numeric',
        ]);

        try {
            // Simpan data film baru ke dalam basis data
            $film = Films::create($request->all());

            return response()->json(['message' => 'Film added successfully', 'film' => $film], 201);
        } catch (\Exception $e) {
            // Tangani jika terjadi kesalahan saat menyimpan data film
            return response()->json(['error' => 'Failed to add film'], 500);
        }
    }

    public function deleteMovie($id)
    {
        try {
            $film = Films::findOrFail($id);
            $film->delete();

            return response()->json(['message' => 'Film deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to delete film'], 500);
        }
    }

}
