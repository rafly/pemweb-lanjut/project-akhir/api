<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index_user()
    {
        $users = User::whereNull('role')->get();

        return response()->json([
            'users' => $users
        ]);
         
    }
    
    //Admin area ---------
    public function index_admin()
    {
        $users = User::where('role', 'like', '%admin%')->get();

        return response()->json([
            'users' => $users
        ]);
    }

    public function deleteAdmin($id)
    {
        $user = User::find($id);

        if (!$user || strpos(strtolower($user->role), 'admin') === false) {
            return response()->json(['message' => 'User not found or not an admin'], 404);
        }

        $user->delete();

        return response()->json(['message' => 'Admin deleted successfully']);
    }

    public function storeAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            \Log::error('Validation failed', $validator->errors()->toArray());
            return response()->json($validator->errors(), 400);
        }

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => 'Admin'
            ]);

            return response()->json(['message' => 'Admin created successfully', 'user' => $user]);
        } catch (\Exception $e) {
            \Log::error('Error creating admin', ['error' => $e->getMessage()]);
            return response()->json(['message' => 'Error creating admin'], 500);
        }
    }

}
