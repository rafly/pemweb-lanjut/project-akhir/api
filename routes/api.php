<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');



Route::get('/dashboard', [DashboardController::class, 'index']);
Route::get('/movie/{id}',[DashboardController::class, 'detail']);
Route::post('/reviews', [DashboardController::class, 'store']);

Route::get('/listUser', [UserController::class, 'index_user']);
Route::get('/listAdmin', [UserController::class, 'index_admin']);
Route::delete('/admin/{id}', [UserController::class, 'deleteAdmin']);
Route::post('/admin', [UserController::class, 'storeAdmin']);
Route::put('/movie/{id}', [DashboardController::class, 'update']);
Route::post('/movie', [DashboardController::class, 'storeMovie']);
Route::delete('/movieDelete/{id}', [DashboardController::class, 'deleteMovie']);

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::delete('logout', [AuthController::class, 'logout']);
    Route::get('refresh', [AuthController::class, 'refresh']);
});

Route::middleware(['auth:api'])->group(function () {
    Route::get('user', function(Request $request) {
        return auth()->user();
    });
    Route::get('/listUser', [UserController::class, 'index_user']);
    Route::get('/listAdmin', [UserController::class, 'index_admin']);
});